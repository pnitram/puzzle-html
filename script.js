var selectedPiece = null;
var lastPlacePiece = null;

var puzzleWidth,
    puzzleHeight,
    pieceWidth,
    pieceHeight;

var magicNumber = 4;
var puzzle;
var pieces = [
        [0, 0],
        [1, 0],
        [2, 0],
        [3, 0],
        
        [0, 1],
        [1, 1],
        [2, 1],
        [3, 1],
        
        [0, 2],
        [1, 2],
        [2, 2],
        [3, 2],
        
        [0, 3],
        [1, 3],
        [2, 3],
        [3, 3]
];
$(function(){
    puzzle = new Image();
    puzzle.addEventListener("load", onImage, false);
    puzzle.src = "puzzle1.jpg";
    
    $("td").click(function(){
        if(selectedPiece != null){
            if(!$(this).find("img").length){
                if(lastPlacePiece != null){
                    lastPlacePiece.html("");
                    lastPlacePiece = null;
                }
                $(this).html("<img src='"+selectedPiece+"'></img>");
                
                selectedPiece = null;
                setCurrentImage(null);
            }
        }else{
            if($(this).find("img").length){
                lastPlacePiece = $(this);
                selectedPiece = $(this).find("img").prop("src");
                setCurrentImage(selectedPiece);
            }   
        }
    });
    
    $("canvas").click(function(){
        if("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAABlCAYAAADkr8m4AAAAXUlEQVR4nO3BMQEAAADCoPVPbQhfoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgNByEAAGDx3xZAAAAAElFTkSuQmCC" == $(this)[0].toDataURL()){
            return;
        }
        if(selectedPiece == null){
            selectedPiece = $(this)[0].toDataURL();
            
            $(this)[0].getContext("2d").clearRect(0, 0, $(this)[0].width, $(this)[0].height);
            setCurrentImage(selectedPiece);
        }

    });
});

function setCurrentImage(src){
    if(src != null){
        $("#currImage").html("<img src='"+src+"' width='"+Math.round(pieceWidth*1.25)+"' height='"+Math.round(pieceHeight*1.25)+"'></img>");
    }else{
        $("#currImage").html("");
    }
}

function onImage(){
    puzzleWidth = puzzle.width;
    puzzleHeight = puzzle.height;
    
    pieceWidth = puzzleWidth / magicNumber;
    pieceHeight = puzzleHeight / magicNumber;
    
    makePieces();
}

function makePieces(){
    for(var i = 0; i < magicNumber; i++){
        for(var j = 0; j < magicNumber; j++){
            var canvas = $("#piece"+i+"_"+j)[0];
            var ctx = canvas.getContext("2d");
            canvas.width = pieceWidth;
            canvas.height = pieceHeight;
            
            var pos = getRandomPos();
            ctx.drawImage(puzzle, pos[0]*pieceWidth, pos[1]*pieceHeight, pieceWidth, pieceHeight, 0, 0, pieceWidth, pieceHeight);
        }
    }          
    setupTable();
}

function getRandomPos(){
    var randpos = Math.floor(Math.random()*pieces.length);
    var rand = pieces[randpos];
    pieces.splice(randpos, 1);
    return rand;
}

function setupTable(){
    $("td").attr("width", pieceWidth);
    $("td").attr("height", pieceHeight);
    
    $("#currImage").css("width", Math.round(pieceWidth*1.25));
    $("#currImage").css("height", Math.round(pieceHeight*1.25));
}